import cv2 as cv
import sys

def imageTest1():
    img = cv.imread(cv.samples.findFile("Images/img1.png"))

    if img is None:
        sys.exit("Could not read the image.")

    cv.imshow("Display window", img)
    k = cv.waitKey(0)

    if k == ord("s"):
        cv.imwrite("Images/img1.jpg", img)

def videoTest1():
    vid = cv.VideoCapture("Videos/Nothing Dear.mp4")

    while vid.isOpened():
        ret, frame = vid.read()

        # if frame is read ret is true
        if not ret:
            print("cant receive frame")
            break
        gray = cv.cvtColor(frame, cv.COLOR_B)

        cv.imshow('frame', gray)
        if cv.waitKey(1) == ord('q'):
            break
    
    vid.release()
    cv.destroyAllWindows()




videoTest1()