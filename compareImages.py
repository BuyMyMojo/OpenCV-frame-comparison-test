import cv2 as cv

# load images
image1 = cv.imread("Images/img1.png")
image2 = cv.imread("Images/img2.png")

# compute difference
difference = cv.subtract(image1, image2)

cv.imwrite('diff.png', difference)
diffImg = cv.cvtColor(difference, cv.COLOR_BGR2GRAY)

nzCount = cv.countNonZero(diffImg)

print("There are "+ str(nzCount) + " different pixels")