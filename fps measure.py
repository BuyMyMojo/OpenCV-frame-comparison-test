import cv2 as cv
from time import time
import csv
import os
import argparse as argp
import ffmpeg
import numpy as np

def main(args):
    # videoToPng(args.Video, args.Output)
    framerateMeasure(args.Output, args.CSV, args.r)
    exit()

def videoToPng(videoPath, pngPath):
    print("path to video: " + videoPath)
    print("Output path: " + pngPath)
    (
    ffmpeg
    .input(videoPath)
    .output(pngPath + '%d.png')
    .run()
    )

def framerateMeasure(pngPath, csvPath, rate):
    start = time()
    differenceValues = []
    for i in range(rate):
        differenceValues.append(0)
    
    fields=['Frame','FPS']
    writeCSV(fields, csvPath)

    j = 1
    # len(os.listdir(pngPath))-1
    for x in range(len(os.listdir(pngPath))-1):
        img1 = cv.imread(pngPath + str(j) + ".jpeg")
        j = j + 1
        img2 = cv.imread(pngPath + str(j) + ".jpeg")
        difference = cv.subtract(img1, img2)
        diffImg = cv.cvtColor(difference, cv.COLOR_BGR2GRAY)
        nzCount = cv.countNonZero(diffImg)

        if nzCount == 0:
            # Dupe Frame
            differenceValues.pop(0)
            differenceValues.append(0)
        else:
            # New frame
            differenceValues.pop(0)
            differenceValues.append(1)

        
        updateCSV(calcFPS(differenceValues), x, csvPath)

    print("Process took "+ str(int(time())-start), " seconds")
    exit()

def calcFPS(diffVal):
    return(np.count_nonzero(diffVal))

def writeCSV(fields, csvPath):
    with open(csvPath, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fields)

        writer.writeheader()

def updateCSV(FPS, frame, csvPath):
    # print("FPS: ", str(FPS), end="\r", flush=True)
    with open(csvPath, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow([frame, FPS])


# setup argparse
parser = argp.ArgumentParser(description='Generate csv file of fps values from lossles video', allow_abbrev=False)

# add arguments
parser.add_argument('Video', metavar='Video', type=str, help='The path to your video file')
parser.add_argument('Output', metavar='Output', type=str, help='The path your image sequence will be saved')
parser.add_argument('CSV', metavar='CSV', type=str, help='The path your csv file will be saved')
parser.add_argument('-r', metavar='FPS', type=int, help='FPS of origional video [default: 60]', default="60")

args = parser.parse_args()


main(args)
